#!/bin/bash
GOPATH=$(pwd) go get github.com/lib/pq
GOPATH=$(pwd) go get gopkg.in/yaml.v2
GOPATH=$(pwd) go get golang.org/x/oauth2
GOPATH=$(pwd) go get github.com/gorilla/websocket
