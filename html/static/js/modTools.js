const formName = document.getElementById('pet-modal-name');
const formImage = document.getElementById('pet-modal-image');
const formBan = document.getElementById('pet-modal-ban');
if(formName){
    formName.addEventListener('submit', function(evt){
        evt.preventDefault();
        const errorMsg = document.getElementById('settings-name-errormsg');
        const result = document.getElementById('settings-nameinput');
        if(result.value == ''){
            errorMsg.innerHTML = 'Your display name can not be empty.';
            errorMsg.classList.add('errormsg-warning');
            return;
        }
        else{
            const xhr = new XMLHttpRequest();
            xhr.open("POST", "/settings/name?petId=" + petId);
            xhr.onreadystatechange = function() {
                if (xhr.readyState === xhr.DONE) {
                    if (xhr.status === 200) {
                        const responseData = JSON.parse(xhr.response);
                        if (responseData.success) {
                            errorMsg.innerHTML = 'Successfully updated the name';
                            errorMsg.classList.add('errormsg-success');
                            document.getElementById('settings-currentname').innerText = result.value;
                        } else {
                            errorMsg.innerHTML = 'Unable to update name: ' + responseData.message;
                            errorMsg.classList.add('errormsg-error');
                        }
                    }
                }
            }
            xhr.onerror = function(event){
                errorMsg.innerHTML = 'Unable to reach server';
                errorMsg.classList.add('errormsg-error');
            }
            const formData = new FormData(formName);
            xhr.send(formData);
        }
    });
}
if(formImage){
    formImage.addEventListener('submit', function(evt){
        evt.preventDefault();
        const errorMsg = document.getElementById('settings-image-errormsg');
        const xhr = new XMLHttpRequest();
        xhr.open("POST", "/settings/image/delete?petId=" + petId);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === xhr.DONE) {
                if (xhr.status === 200) {
                    const responseData = JSON.parse(xhr.response);
                    if (responseData.success) {
                        errorMsg.innerHTML = 'Successfully removed the image';
                        errorMsg.classList.add('errormsg-success');
                        document.getElementById('settings-currentname').innerText = result.value;
                    } else {
                        errorMsg.innerHTML = 'Unable to remove the image: ' + responseData.message;
                        errorMsg.classList.add('errormsg-error');
                    }
                }
            }
        }
        xhr.onerror = function(event){
            errorMsg.innerHTML = 'Unable to reach server';
            errorMsg.classList.add('errormsg-error');
        }
        const formData = new FormData(formName);
        xhr.send(formData);
    });
}
if(formBan){
    formBan.addEventListener('submit', function(evt){
        evt.preventDefault();
        const errorMsg = document.getElementById('pet-ban-error');
        const xhr = new XMLHttpRequest();
        xhr.open("POST", "/settings/ban?petId=" + petId);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === xhr.DONE) {
                if (xhr.status === 200) {
                    const responseData = JSON.parse(xhr.response);
                    if (responseData.success) {
                        errorMsg.innerHTML = 'Successfully banned the user';
                        errorMsg.classList.add('errormsg-success');
                        document.getElementById('settings-currentname').innerText = result.value;
                    } else {
                        errorMsg.innerHTML = 'Unable to ban the user: ' + responseData.message;
                        errorMsg.classList.add('errormsg-error');
                    }
                }
            }
        }
        xhr.onerror = function(event){
            errorMsg.innerHTML = 'Unable to reach server';
            errorMsg.classList.add('errormsg-error');
        }
        const formData = new FormData(formName);
        xhr.send(formData);
    });
}