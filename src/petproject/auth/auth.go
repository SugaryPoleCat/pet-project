package auth

import (
	"log"
	"strings"
	"strconv"
	"time"
	"database/sql"
	"petproject/account"
	"petproject/accountcollection"
	"petproject/db"
	"petproject/util"
)

type dbStatements struct {
	selectToken *sql.Stmt
	insertToken *sql.Stmt
	updateTokenTimeout *sql.Stmt
	deleteToken *sql.Stmt
	maintenanceToken *sql.Stmt
}

var stmts *dbStatements = nil

func openDb() *sql.DB {
	db := db.GetConnection()
	if stmts == nil {
		stmts = new(dbStatements)
		var err error
		stmts.selectToken, err = db.Prepare("SELECT timeout FROM tokens WHERE user_id=$1 AND token=$2")
		if err != nil {
			log.Print("Failed to prepare statement select token ", err)
		}
		stmts.insertToken, err = db.Prepare("INSERT INTO tokens (user_id, token) VALUES ($1, $2)")
		if err != nil {
			log.Print("Failed to prepare statement insert token ", err)
		}
		stmts.updateTokenTimeout, err = db.Prepare("UPDATE tokens SET timeout = NOW() + INTERVAL '1 month' WHERE user_id=$1 AND token=$2")
		if err != nil {
			log.Print("Failed to prepare statement update token timeout ", err)
		}
		stmts.deleteToken, err = db.Prepare("DELETE FROM tokens WHERE user_id=$1 AND token=$2")
		if err != nil {
			log.Print("Failed to prepare statement delete token ", err)
		}
		stmts.maintenanceToken, err = db.Prepare("DELETE FROM tokens WHERE timeout < NOW() RETURNING token")
		if err != nil {
			log.Print("Failed to prepare statement maintenance token ", err)
		}
	}
	return db
}

var cacheTokenValid = map[string]int64{}

func GetAccountByToken(token string, refresh bool) *account.Account {
	if !strings.Contains(token, "|") {
		return nil
	}
	parts := strings.Split(token, "|")
	id, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil
	}
	curTime := time.Now().Unix()
	if val, ok := cacheTokenValid[token]; ok && val > curTime {
		return accountcollection.Get(id)
	}
	openDb()
	var timeout time.Time
	err = stmts.selectToken.QueryRow(id, token).Scan(&timeout)
	if err != nil {
		// not found
		return nil
	}
	cacheTokenValid[token] = timeout.Unix()
	if timeout.Unix() < curTime {
		return nil
	}
	if refresh {
		stmts.updateTokenTimeout.Exec(id, token)
	}
	return accountcollection.Get(id)
}

func GetGuestAccount(ident string) *account.Account {
	if ident == "" {
		return nil
	}
	return accountcollection.GetOrCreateAuth(account.LoginGuest, ident)
}

func DeleteToken(token string) {
	delete(cacheTokenValid, token)
	if !strings.Contains(token, "|") {
		return
	}
	parts := strings.Split(token, "|")
	id, err := strconv.Atoi(parts[0])
	if err != nil {
		return
	}
	openDb()
	stmts.deleteToken.Exec(id, token)
}

func Maintenance() {
	openDb()
	rows, err := stmts.maintenanceToken.Query()
	if err != nil {
		log.Print("Error doing auth maintenance: ", err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var token string
		err = rows.Scan(&token)
		if err != nil {
			log.Print("Error doing auth maintenance: ", err)
			return
		}
		delete(cacheTokenValid, token)
	}
}

func CreateCookieToken(a *account.Account) string {
	openDb()
	randStr := util.RandomString(64)
	id := a.GetId()
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)
	cookieToken := strconv.FormatInt(int64(id), 10) + "|" + timestamp + randStr
	_, err := stmts.insertToken.Exec(id, cookieToken)
	if err != nil {
		log.Print("Error inserting token into DB: ", err)
	}
	return cookieToken
}
