package db

import (
	"log"
	"database/sql"
	_ "github.com/lib/pq"
	"strconv"
	"io/ioutil"
	"strings"
	"os"

	"petproject/config"
)

var db_cache *sql.DB = nil
var TARGET_SCHEMA int = 2

func GetConnection() *sql.DB {
	if db_cache != nil {
		return db_cache
	}
	db, err := sql.Open("postgres", config.Get().Database.Postgres)
	if err != nil {
		log.Fatal(err)
	}
	db_cache = db
	return db_cache
}

func getSchemaVersion() int {
	db := GetConnection()
	res, err := db.Query("SELECT version FROM schema");
	if err != nil {
		return 0
	}
	defer res.Close()
	if !res.Next() {
		return 0
	}
	var version int
	err = res.Scan(&version)
	if err != nil {
		return 0
	}
	
	return version
}

func setSchemaVersion(version int) {
	db := GetConnection()
	_, err := db.Exec("UPDATE schema SET version = $1", version)
	if err != nil {
		log.Print("Failed to update schema version")
		log.Fatal(err)
	}
}

func Startup() {
	db := GetConnection()
	version := getSchemaVersion()
	for version < TARGET_SCHEMA {
		version++
		filename := "schema/v" + strconv.Itoa(version) + ".sql"
		log.Print("Upgrading schema to ", filename)
		file, err := os.Open(filename)
		if err != nil {
			log.Print("Failed to open schema file")
			log.Fatal(err)
		}
		filecontent, err := ioutil.ReadAll(file)
		if err != nil {
			log.Print("Failed to read schema file")
			log.Fatal(err)
		}
		requests := strings.Split(string(filecontent), ";")
		for _, request := range requests {
			_, err = db.Exec(request)
			if err != nil {
				log.Print("Failed to update schema")
				log.Print(request)
				log.Fatal(err)
			}
		}
		setSchemaVersion(version)
	}
	log.Print("Database is up-to-date!")
}
