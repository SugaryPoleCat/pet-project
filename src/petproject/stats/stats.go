package stats

import (
	"log"
	"database/sql"
	"petproject/db"
	"petproject/util"
	"time"
	"fmt"
)

type dbStatements struct {
	loadMostPerTime *sql.Stmt
	updateMostPerTime *sql.Stmt
	insertMostPerTime *sql.Stmt
	
	loadPetsPerTime *sql.Stmt
	updatePetsPerTime *sql.Stmt
	insertPetsPerTime *sql.Stmt
	
	getCachePets *sql.Stmt
	updateCachePets *sql.Stmt
	insertCachePets *sql.Stmt
	purgeCachePets *sql.Stmt
}

var stmts *dbStatements = nil

func openDb() *sql.DB {
	db := db.GetConnection()
	if stmts == nil {
		stmts = new(dbStatements)
		var err error
		stmts.loadMostPerTime, err = db.Prepare("SELECT other_id, amount, place FROM stats_most_per_time WHERE time_span=$1 AND time_ident=$2 AND given_type=$3 ORDER BY place ASC LIMIT $4")
		if err != nil {
			log.Print("Failed to prepare statement stats loadMostPerTime ", err)
		}
		stmts.updateMostPerTime, err = db.Prepare("UPDATE stats_most_per_time SET amount=$2, place=$3 WHERE time_span=$4 AND other_id=$1 AND time_ident=$5 AND given_type=$6")
		if err != nil {
			log.Print("Failed to prepare statement stats updateMostPerTime ", err)
		}
		stmts.insertMostPerTime, err = db.Prepare("INSERT INTO stats_most_per_time (other_id, amount, place, time_span, time_ident, given_type) VALUES ($1, $2, $3, $4, $5, $6)")
		if err != nil {
			log.Print("Failed to prepare statement stats insertMostPerTime ", err)
		}
		
		stmts.loadPetsPerTime, err = db.Prepare("SELECT amount FROM stats_pets_per_time WHERE time_ident=$1")
		if err != nil {
			log.Print("Failed to prepare statement stats loadPetsPerTime ", err)
		}
		stmts.updatePetsPerTime, err = db.Prepare("UPDATE stats_pets_per_time SET amount=$1 WHERE time_ident=$2")
		if err != nil {
			log.Print("Failed to prepare statement stats updatePetsPerTime ", err)
		}
		stmts.insertPetsPerTime, err = db.Prepare("INSERT INTO stats_pets_per_time (amount, time_ident) VALUES($1, $2)")
		if err != nil {
			log.Print("Failed to prepare statement stats insertPetsPerTime ", err)
		}
		
		stmts.getCachePets, err = db.Prepare("SELECT amount FROM stats_cache_pets WHERE other_id=$1 AND time_span=$2 AND given_type=$3")
		if err != nil {
			log.Print("Failed to prepare statement stats getCachePets ", err)
		}
		stmts.updateCachePets, err = db.Prepare("UPDATE stats_cache_pets SET amount=$1 WHERE other_id=$2 AND time_span=$3 AND given_type=$4")
		if err != nil {
			log.Print("Failed to prepare statement stats updateCachePets ", err)
		}
		stmts.insertCachePets, err = db.Prepare("INSERT INTO stats_cache_pets (amount, other_id, time_span, given_type) VALUES ($1, $2, $3, $4)")
		if err != nil {
			log.Print("Failed to prepare statement stats insertCachePets ", err)
		}
		stmts.purgeCachePets, err = db.Prepare("DELETE FROM stats_cache_pets WHERE time_span=$1")
		if err != nil {
			log.Print("Failed to prepare statement stats purgeCachePets ", err)
		}
	}
	return db
}

type timespanType int
const (
	TimespanDay = 0
	TimespanWeek = 1
	TimespanMonth = 2
	TimespanYear = 3
	TimespanAlltime = 4
)

type givenType int
const (
	GivenTypeReceived = 0
	GivenTypeGiven = 1
)

type StatsAction int
const (
	ActionFlushCache = 0
	ActionSave = 1
)

var allTimespans = []timespanType{TimespanDay, TimespanWeek, TimespanMonth, TimespanYear, TimespanAlltime}
var allTimespansMap = map[string]timespanType{
	"day": TimespanDay,
	"week": TimespanWeek,
	"month": TimespanMonth,
	"year": TimespanYear,
	"alltime": TimespanAlltime,
}
var allTimespansString = []string{"day", "week", "month", "year", "alltime"}
var allGivenTypes = []givenType{GivenTypeReceived, GivenTypeGiven}
var allGivenTypesMap = map[string]givenType{
	"received": GivenTypeReceived,
	"given": GivenTypeGiven,
}
var allGivenTypesString = []string{"received", "given"}

var maxTimespanPerTime = map[timespanType]int{
	TimespanDay: 1,
	TimespanWeek: 1,
	TimespanMonth: 10,
	TimespanYear: 50,
	TimespanAlltime: 100,
}

var timeIdentsLut = map[timespanType]func(time.Time)string{
	TimespanDay: func(t time.Time) string {
		year, month, day := t.Date()
		return fmt.Sprintf("d%04d%02d%02d", year, month, day)
	},
	TimespanWeek: func(t time.Time) string {
		year, week := t.ISOWeek()
		return fmt.Sprintf("w%04d%02d", year, week)
	},
	TimespanMonth: func(t time.Time) string {
		year, month, _ := t.Date()
		return fmt.Sprintf("m%04d%02d", year, month)
	},
	TimespanYear: func(t time.Time) string {
		year, _, _ := t.Date()
		return fmt.Sprintf("y%04d", year)
	},
	TimespanAlltime: func(t time.Time) string {
		return "a"
	},
}

type statsPair struct {
	Id int
	Amount int64
}

type StatsRegister struct {
	A int
	P int
	Method util.PetMethod
}

type stats struct {
	curTime time.Time
	register chan *StatsRegister
	action chan StatsAction
	mostTime map[givenType]map[timespanType][]*statsPair
	cachePets map[givenType]map[timespanType]map[int]int64
	petsPerTime map[timespanType]int64
}

type TemplateStats struct {
	Time time.Time
	Leaderboard map[givenType]map[timespanType][]*statsPair
	Timespans []string
	GivenTypes []string
}

func (s *stats) getCachePets(g givenType, t timespanType, i int) int64 {
	if val, ok := s.cachePets[g][t][i]; ok {
		return val
	}
	var amount int64;
	err := stmts.getCachePets.QueryRow(i, t, g).Scan(&amount)
	if err != nil {
		s.cachePets[g][t][i] = int64(0)
		return int64(0)
	}
	s.cachePets[g][t][i] = amount
	return amount
}

func (s *stats) setCachePets(g givenType, t timespanType, i int, amount int64) {
	s.cachePets[g][t][i] = amount
}

func (s *stats) purgeCache(t timespanType) {
	stmts.purgeCachePets.Exec(t)
	for _, given := range allGivenTypes {
		s.cachePets[given][t] = make(map[int]int64)
	}
}

func (s *stats) purge() {
	curTime := time.Now().UTC()
	for _, timespan := range allTimespans {
		thisTimeIdent := timeIdentsLut[timespan](curTime)
		lastTimeIdent := timeIdentsLut[timespan](s.curTime)
		if thisTimeIdent != lastTimeIdent {
			s.purgeCache(timespan)
			s.petsPerTime[timespan] = 0
		}
	}
}

func (s *stats) updateMostGiven(given givenType, id int) {
	for _, timespan := range allTimespans {
		amount := s.getCachePets(given, timespan, id) + 1
		s.setCachePets(given, timespan, id, amount)
		insertId := 0
		for _, pair := range s.mostTime[given][timespan] {
			if amount > pair.Amount {
				break
			}
			insertId++
		}
		maxLen := maxTimespanPerTime[timespan]
		if insertId < maxLen {
			slice := s.mostTime[given][timespan]
			slice = append(slice, nil)
			copy(slice[insertId+1:], slice[insertId:])
			slice[insertId] = &statsPair{
				Id: id,
				Amount: amount,
			}
			// filter out potential duplicates
			for {
				deleteId := -1
				for i, pair := range slice {
					if i > insertId && pair.Id == id {
						deleteId = i
						break
					}
				}
				if deleteId == -1 {
					break
				}
				slice = append(slice[:deleteId], slice[deleteId+1:]...)
			}
			// truncate if needed
			if len(slice) >= maxLen {
				slice = slice[:maxLen]
			}
			s.mostTime[given][timespan] = slice
		}
	}
}

func (s *stats) handleRegister(reg *StatsRegister) {
	curTime := time.Now().UTC()
	_, _, day := curTime.Date()
	_, _, lastDay := s.curTime.Date()
	if day != lastDay {
		s.purge()
		s.save()
		s.load()
	}
	petId := reg.P
	accountId := reg.A
	s.updateMostGiven(GivenTypeReceived, petId)
	if accountId != -1 {
		s.updateMostGiven(GivenTypeGiven, accountId)
	}
	for _, timespan := range allTimespans {
		s.petsPerTime[timespan]++
	}
}

func (s *stats) run() {
	for {
		select {
		case reg := <-s.register:
			// ok, we have a new stat! let's register it!
			s.handleRegister(reg)
		case action := <-s.action:
			switch action {
			case ActionFlushCache:
				s.flushCache()
			case ActionSave:
				s.save()
			}
		}
	}
}

func (s *stats) save() {
	for _, given := range allGivenTypes {
		for _, timespan := range allTimespans {
			timeIdent := timeIdentsLut[timespan](s.curTime)
			for place, pair := range s.mostTime[given][timespan] {
				_, err := stmts.insertMostPerTime.Exec(pair.Id, pair.Amount, place, timespan, timeIdent, given)
				if err != nil {
					_, err = stmts.updateMostPerTime.Exec(pair.Id, pair.Amount, place, timespan, timeIdent, given)
					if err != nil {
						log.Print("Failed to save stats: ", err)
					}
				}
			}
		}
	}
	for _, timespan := range allTimespans {
		timeIdent := timeIdentsLut[timespan](s.curTime)
		amount := s.petsPerTime[timespan]
		_, err := stmts.insertPetsPerTime.Exec(amount, timeIdent)
		if err != nil {
			_, err := stmts.updatePetsPerTime.Exec(amount, timeIdent)
			if err != nil {
				log.Print("Failed to save stats: ", err)
			}
		}
	}
}

func (s *stats) loadRows(rows *sql.Rows, err error) []*statsPair {
	slice := make([]*statsPair, 0)
	if err != nil {
		log.Print("Error loading data: ", err)
		return slice
	}
	defer rows.Close()
	var place int
	for rows.Next() {
		pair := &statsPair{-1, 0}
		rows.Scan(&pair.Id, &pair.Amount, &place)
		slice = append(slice, pair)
	}
	return slice
}

func (s *stats) load() {
	s.curTime = time.Now().UTC()
	
	for _, given := range allGivenTypes {
		for _, timespan := range allTimespans {
			timeIdent := timeIdentsLut[timespan](s.curTime)
			s.mostTime[given][timespan] = s.loadRows(stmts.loadMostPerTime.Query(timespan, timeIdent, given, maxTimespanPerTime[timespan]))
		}
	}
	
	for _, timespan := range allTimespans {
		timeIdent := timeIdentsLut[timespan](s.curTime)
		var val int64
		err := stmts.loadPetsPerTime.QueryRow(timeIdent).Scan(&val)
		if err != nil {
			log.Print("Failed to load stats: ", err)
		}
		s.petsPerTime[timespan] = val
	}
}

func GetTemplate() *TemplateStats {
	t := new(TemplateStats)
	t.Time = statsHandler.curTime
	t.Leaderboard = statsHandler.mostTime
	t.Timespans = allTimespansString
	t.GivenTypes = allGivenTypesString
	return t
}

func (t *TemplateStats) GetEntries(giveType string, timespan string) []*statsPair {
	return t.Leaderboard[allGivenTypesMap[giveType]][allTimespansMap[timespan]]
}

var statsHandler *stats = nil
func Register(reg *StatsRegister) {
	statsHandler.register <- reg
}

func Action(a StatsAction) {
	statsHandler.action <- a
}

func FlushAll() {
	statsHandler.flushCache()
	statsHandler.save()
}

func (s *stats) flushCache() {
	for _, given := range allGivenTypes {
		for _, timespan := range allTimespans {
			for id, amount := range s.cachePets[given][timespan] {
				_, err := stmts.insertCachePets.Exec(amount, id, timespan, given)
				if err != nil {
					_, err = stmts.updateCachePets.Exec(amount, id, timespan, given)
					if err != nil {
						log.Print("Error flushing cache: ", err)
					}
				}
			}
		}
	}
}

func Load() {
	openDb()
	statsHandler = new(stats)
	statsHandler.register = make(chan *StatsRegister)
	statsHandler.action = make(chan StatsAction)
	statsHandler.petsPerTime = make(map[timespanType]int64)
	statsHandler.mostTime = make(map[givenType]map[timespanType][]*statsPair)
	statsHandler.cachePets = make(map[givenType]map[timespanType]map[int]int64)
	for _, given := range allGivenTypes {
		statsHandler.mostTime[given] = make(map[timespanType][]*statsPair)
		statsHandler.cachePets[given] = make(map[timespanType]map[int]int64)
		for _, timespan := range allTimespans {
			statsHandler.mostTime[given][timespan] = make([]*statsPair, 0)
			statsHandler.cachePets[given][timespan] = make(map[int]int64)
		}
	}
	statsHandler.load()
	go statsHandler.run()
}
