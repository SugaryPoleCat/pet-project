package websocketserver

import (
	"petproject/util"
)

type Hub struct {
	clients map[*Client]bool
	register chan *Client
	unregister chan *Client
	broadcastPet chan *petMessage
}

type petMessage struct {
	id int
	msg string
}

func newHub() *Hub {
	return &Hub{
		clients: make(map[*Client]bool),
		register: make(chan *Client),
		unregister: make(chan *Client),
		broadcastPet: make(chan *petMessage),
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case petMsg := <-h.broadcastPet:
			for c := range h.clients {
				if util.IntInArray(petMsg.id, c.petSubs) {
					c.send <- petMsg.msg
				}
			}
		}
	}
}
