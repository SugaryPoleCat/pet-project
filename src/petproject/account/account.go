package account

import (
	"time"
	"log"
	"database/sql"
	"petproject/pet"
	"petproject/petcollection"
	"petproject/util"
	"petproject/stats"
	"encoding/json"
	"petproject/db"
	"strings"
)

type LoginMethod int
const (
	LoginGuest = 0
	LoginGoogle = 1
	LoginFacebook = 2
	LoginGithub = 3
	LoginVk = 4
	LoginDiscord = 5
	LoginTwitter = 6
	LoginReddit = 7
	LoginTumblr = 8
	LoginGitlab = 9
)

type Account struct {
	loginMethod LoginMethod
	id int
	auth string
	xp int64
	name string
	cooldown map[util.PetMethod]int64
	nextAppease map[util.PetMethod]int64
	petsGiven map[util.PetMethod]int64
	petsGivenAt time.Time
	petsGivenTotal int64
	havedb bool
	changed bool
	role string
}

type TemplateAccount struct {
	Id int
	Name string
	Xp int64
	PetsGiven map[util.PetMethod]int64
	Pet *pet.TemplatePet
	Cooldown map[util.PetMethod]int64
	PetsGivenAt time.Time
	PetsGivenTotal int64
	HavePermission func(string) bool
}

type dbStatements struct {
	selectAccountID *sql.Stmt
	selectAccountAuth *sql.Stmt
	updateAccount *sql.Stmt
	updateAccountAuth *sql.Stmt
	insertAccount *sql.Stmt
	maintenanceAccount *sql.Stmt
}

var stmts *dbStatements = nil

func openDb() *sql.DB {
	db := db.GetConnection()
	if stmts == nil {
		stmts = new(dbStatements)
		var err error
		stmts.selectAccountID, err = db.Prepare("SELECT id, auth, loginMethod, xp, name, cooldown, nextAppease, petsGiven, petsGivenAt, petsGivenTotal, role FROM accounts WHERE id=$1")
		if err != nil {
			log.Print("Failed to prepare statement select account id ", err)
		}
		stmts.selectAccountAuth, err = db.Prepare("SELECT id, auth, loginMethod, xp, name, cooldown, nextAppease, petsGiven, petsGivenAt, petsGivenTotal, role FROM accounts WHERE loginMethod=$1 AND auth=$2")
		if err != nil {
			log.Print("Failed to prepare statement select account auth ", err)
		}
		stmts.updateAccount, err = db.Prepare("UPDATE accounts SET xp=$2, name=$3, cooldown=$4, nextAppease=$5, petsGiven=$6, petsGivenAt=$7, petsGivenTotal=$8, role=$9 WHERE id=$1")
		if err != nil {
			log.Print("Failed to prepare statement update account ", err)
		}
		stmts.updateAccountAuth, err = db.Prepare("UPDATE accounts SET name=$2, loginMethod=$3, auth=$4, created_at=NOW() WHERE id=$1")
		if err != nil {
			log.Print("Failed to prepare statement update account auth ", err)
		}
		stmts.insertAccount, err = db.Prepare("INSERT INTO accounts (loginMethod, auth) VALUES ($1, $2) RETURNING id")
		if err != nil {
			log.Print("Failed to prepare statement insert account ", err)
		}
		stmts.maintenanceAccount, err = db.Prepare("DELETE FROM accounts WHERE loginMethod=$1 AND created_at < NOW() - INTERVAL '10 day' RETURNING id")
		if err != nil {
			log.Print("Failed to prepare statement maintenance account ", err)
		}
	}
	return db
}

func (a *Account) GetPet() *pet.Pet {
	if a.loginMethod == LoginGuest || a.name == "" {
		return nil
	}
	return petcollection.GetOrCreate(a.id, a.name)
}

func (a *Account) GetTemplate() *TemplateAccount {
	t := new(TemplateAccount)
	t.Id = a.id
	t.Name = a.name
	t.Xp = a.xp
	t.PetsGiven = a.petsGiven
	p := a.GetPet()
	if p != nil {
		t.Pet = p.GetTemplate()
	}
	t.Cooldown = a.cooldown
	t.PetsGivenAt = a.petsGivenAt
	t.PetsGivenTotal = a.petsGivenTotal
	t.HavePermission = func (perm string) bool {
		return a.HavePermission(perm)
	}
	return t
}

func (a *Account) GetCooldown(method util.PetMethod) int64 {
	return util.GetFromMap(a.nextAppease, method, 0)
}

func (t *TemplateAccount) GetMaxCooldown(method util.PetMethod) int64 {
	return util.GetFromMap(t.Cooldown, method, util.DefaultCooldowns[method]) / 1e6
}

func (a *Account) Appease(method util.PetMethod, p *pet.Pet) bool {
	// check if it is a valid pet method
	if !util.IsValidPetMethod(method) {
		return false
	}
	// check if it is our pet
	if p.GetId() == a.id {
		return false
	}
	// check if we are still on CD
	curTime := time.Now().UnixNano()
	appeaseTime := a.GetCooldown(method)
	if appeaseTime > curTime {
		return false
	}
	log.Print("Performing appease ", method)
	// perform the actual pet
	p.Pet(method)
	// set next CD
	a.nextAppease[method] = curTime + util.GetFromMap(a.cooldown, method, util.DefaultCooldowns[method])
	// increase pets given counter and xp
	a.petsGiven[method] = util.GetFromMap(a.petsGiven, method, 0)
	a.petsGiven[method]++
	a.xp += util.PetMethodXP[method]
	a.petsGivenAt = time.Now().UTC()
	a.petsGivenTotal++
	
	statsId := a.id
	if !a.LoggedIn() {
		statsId = -1
	}
	
	stats.Register(&stats.StatsRegister{
		A: statsId,
		P: p.GetId(),
		Method: method,
	})
	
	a.changed = true
	return true
}

func (a *Account) SetAuth(method LoginMethod, auth string) {
	a.loginMethod = method
	a.auth = auth
	a.changed = true
	a.SaveAuth()
}

func (a *Account) SetName(name string) (bool, string) {
	// TODO: bad word filtering
	if len(name) < 3 {
		return false, "Name too short"
	}
	if len(name) > 32 {
		return false, "Name too long"
	}
	if util.StringInArray(strings.ToLower(name), util.InvalidPetNameUrls) {
		return false, "Name reserved"
	}
	
	a.name = name
	a.changed = true
	a.Save()
	p := a.GetPet()
	p.SetName(a.name)
	p.Save()
	return true, ""
}

func (a *Account) SetRole(role string) {
	a.role = role
	a.changed = true
	a.Save()
}

func (a *Account) IsAuth(method LoginMethod, auth string) bool {
	return a.loginMethod == method && a.auth == auth
}

func (a *Account) GetId() int {
	return a.id
}

func (a *Account) GetName() string {
	return a.name
}

func (a *Account) SaveAuth() {
	openDb()
	var err error
	a.Save()
	_, err = stmts.updateAccountAuth.Exec(a.id, a.name, a.loginMethod, a.auth)
	if err != nil {
		log.Print("Error saving account auth: ", err)
	}
}

func (a *Account) HavePermission(perm string) bool {
	permMap := map[string]int {
		"BANNED": 0,
		"USER": 1,
		"MODERATOR": 2,
		"ADMIN": 3,
	}
	return permMap[a.role] >= permMap[perm]
}

func (a *Account) Save() {
	if !a.changed {
		return
	}
	openDb()
	var err error
	if !a.havedb || a.id == -1 {
		// ok, create it first
		err = stmts.insertAccount.QueryRow(a.loginMethod, a.auth).Scan(&a.id)
		if err != nil {
			log.Print("Failed to insert new account ", err)
		}
		a.havedb = true
	}
	jsonCooldown, err := json.Marshal(a.cooldown)
	if err != nil {
		log.Print("Failed to json-ify cooldown data", err)
		a.changed = false
		return
	}
	jsonNextAppease, err := json.Marshal(a.nextAppease)
	if err != nil {
		log.Print("Failed to json-ify next appease data", err)
		a.changed = false
		return
	}
	jsonPetsGiven, err := json.Marshal(a.petsGiven)
	if err != nil {
		log.Print("Failed to json-ify pets given data", err)
		a.changed = false
		return
	}
	_, err = stmts.updateAccount.Exec(a.id, a.xp, a.name, string(jsonCooldown), string(jsonNextAppease), string(jsonPetsGiven), a.petsGivenAt, a.petsGivenTotal, a.role)
	if err != nil {
		log.Print("Failed to update account ", err)
	}
	a.changed = false
}

func (a *Account) LoggedIn() bool {
	return a.loginMethod != LoginGuest
}

func FromDbId(id int) *Account {
	openDb()
	res, err := stmts.selectAccountID.Query(id)
	if err != nil {
		defer res.Close()
		log.Print("Failed to get account with id ", id, ": ", err)
		return nil
	}
	return FromDBData(res)
}

func FromDbAuth(method LoginMethod, auth string) *Account {
	openDb()
	res, err := stmts.selectAccountAuth.Query(method, auth)
	if err != nil {
		defer res.Close()
		log.Print("Failed to get account with auth ", method, " ", auth, ": ", err)
		return nil
	}
	return FromDBData(res)
}

func FromDBData(res *sql.Rows) *Account {
	defer res.Close()
	if !res.Next() {
		log.Print("Couldn't get non-existing account")
		return nil
	}
	a := New("")
	a.changed = false
	a.havedb = true
	var jsonCooldown []byte
	var jsonNextAppease []byte
	var jsonPetsGiven []byte
	err := res.Scan(&a.id, &a.auth, &a.loginMethod, &a.xp, &a.name, &jsonCooldown, &jsonNextAppease, &jsonPetsGiven, &a.petsGivenAt, &a.petsGivenTotal, &a.role)
	if err != nil {
		log.Print("Failed to get account ", err)
		return nil
	}
	err = json.Unmarshal(jsonCooldown, &a.cooldown)
	if err != nil {
		log.Print("Failed to de-json-ify account cooldown data")
	}
	err = json.Unmarshal(jsonNextAppease, &a.nextAppease)
	if err != nil {
		log.Print("Failed to de-json-ify account next appease data")
	}
	err = json.Unmarshal(jsonPetsGiven, &a.petsGiven)
	if err != nil {
		log.Print("Failed to de-json-ify account pets given data")
	}
	return a
}

func New(auth string) *Account {
	a := new(Account)
	a.id = -1
	a.loginMethod = LoginGuest
	a.name = ""
	a.xp = 0
	a.cooldown = make(map[util.PetMethod]int64)
	a.nextAppease = make(map[util.PetMethod]int64)
	a.petsGiven = make(map[util.PetMethod]int64)
	a.petsGivenAt = time.Unix(0, 0).UTC()
	a.petsGivenTotal = 0
	a.havedb = false
	a.changed = true
	a.auth = auth
	a.role = "USER"
	return a
}

func Maintenance() []int {
	openDb()
	var ids []int
	rows, err := stmts.maintenanceAccount.Query(LoginGuest)
	if err != nil {
		log.Print("Error during account maintenance: ", err)
		return ids
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		err = rows.Scan(&id)
		if err != nil {
			log.Print("Error during account maintenance: ", err)
			return ids
		}
		ids = append(ids, id)
	}
	return ids
}
